# Skill demontrations - Our Students

## Task descrition

Your task is to implement two designs from Figma

- "main page" (Desktop view) - "All frames" Page from Pages section (top left corner)
- "main page mob" (Mobile view)
- "catalog"
- "catalog mob one column category"

## Figma design link

[Figma design link is here](https://www.figma.com/file/nm0tngMap2yDdPLsypNMfW/Instagram-Carousels-(Community))

## Font Poppins

[fonts google link](https://fonts.google.com/specimen/Poppins?query=poppins)

## Image design

<details>
<summary>Click to display final design (first screen)</summary>

![image info](assets/design/1.jpg)

</details>

<details>
<summary>Click to display final design (second screen)</summary>

![image info](assets/design/2.jpg)

</details>

<details>
<summary>Click to display final design (thrid screen)</summary>

![image info](assets/design/3.jpg)

</details>

<details>
<summary>Click to display final design (last screen)</summary>

![image info](assets/design/8.jpg)

</details>

## Project structure

`index.html` - contains HTML code

`styles` - contains CSS code

`assets/design` - contains final design template

`assets/images` - contains images

`assets/icons` - contains icons

`partials/{surname}` - contains isolated code for each students

## How to run

### Running by simply opening index.html in browser

- Open file index.html in browser
  - In Visual Studio Code right click in index.html -> Copy Path
  - Open browser and paste copied path as an url (i.e `D://Proejcts/intro-to-web-dev-tasks/packages/classtask-YuliaVorman/index.html`)
- Enjoy

Why not to use that approach - every time you're making changes you have to reload your page manually

Why to use - simply you don't want to deal with NPM and JavaScript or there might be any issues currently with it

### Running using NPM and JavaScript (Preferred)

- Open console
- In console navigate to the project root (`./intro-to-web-dev-tasks`)
- Run `npm install`
- Navigate to the project `cd packages/classtask-YuliaVorman`
- Run `npm run serve`
- Open browser
- Navigate to `http://localhost:8080/`
- Enjoy

Why to use - every time you make a change your page is automatically reloaded and your changes applied

Why not to use - you don't want to deal with NPM and JavaScript or there might be any issues currently with it
